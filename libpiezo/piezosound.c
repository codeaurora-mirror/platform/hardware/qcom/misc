/*
*
* Copyright (c) 2015, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#include <cutils/log.h>

#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <poll.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <hardware/piezosound.h>


/******************************************************************************/

#define PIEZO_COMMAND_ON  1
#define PIEZO_COMMAND_OFF 0

static pthread_once_t g_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t g_monitor_thread;
static int g_monitor_thread_running = 0;
static int g_command_fd = 0;
static piezo_cmd_done_callback g_cmd_done_cb = NULL;


static const char* PIEZO_FREQUENCY_FILE = "/sys/kernel/piezo/frequency";
static const char* PIEZO_DURATION_FILE = "/sys/kernel/piezo/duration";
static const char* PIEZO_DUTY_CYCLE_FILE = "/sys/kernel/piezo/duty_cycle";
static const char* PIEZO_COMMAND_FILE = "/sys/kernel/piezo/command";

/******************************************************************************/

void init_globals(void)
{
    pthread_mutex_init(&g_mutex, NULL);
}

static int write_int(char const* path, int value)
{
    int fd;
    static int already_warned = 0;

    fd = open(path, O_RDWR);
    if (fd >= 0) {
        char buffer[20];
        int bytes = sprintf(buffer, "%d\n", value);
        int amt = write(fd, buffer, bytes);
        close(fd);
        return amt == -1 ? -errno : 0;
    } else {
        if (already_warned == 0) {
            ALOGE("write_int failed to open %s\n", path);
            already_warned = 1;
        }
        return -errno;
    }
}

static void cleanup_thread(void)
{
    g_monitor_thread_running = 0;
    close(g_command_fd);
}


void monitor_thread_cleanup(void *data)
{
   (void)data;
   cleanup_thread();
}

void *monitor_command_thread(void *data)
{
    struct pollfd fd;
    int bytes, err;
    char buffer[20];
    void *arg = NULL;

    (void)data;
    pthread_mutex_lock(&g_mutex);
    g_monitor_thread_running = 1;
    pthread_cleanup_push(monitor_thread_cleanup, arg);

    /* Initiates ON command to Kernel */
    g_command_fd = open(PIEZO_COMMAND_FILE, O_RDWR);
    bytes = sprintf(buffer, "%d\n", PIEZO_COMMAND_ON);
    bytes = write(g_command_fd, buffer, bytes);

    ALOGD("%s:write command result: %d\n", __func__, bytes);

    fd.fd = g_command_fd;
    fd.events = POLLERR|POLLPRI;

     /* Dummy read for poll */
    bytes = read( g_command_fd, buffer, 20);
    fd.revents = 0;

    pthread_mutex_unlock(&g_mutex);

    /* Start polling to send another request */
    err = poll(&fd, 1, -1);
    if (err == -1) {
        ALOGE("%s:Error poll for command off \n", __func__);
    }

    ALOGD("%s:polling on command off \n", __func__);

    if(fd.revents & (POLLERR|POLLPRI) )
    {
       ALOGE("%s:on command done in kernel\n", __func__);
       if (g_cmd_done_cb != NULL) {
           g_cmd_done_cb(arg);
       } else {
           ALOGE("%s: callback NULL \n", __func__);
       }
    }

    pthread_cleanup_pop(1);

    return NULL;
}


static int register_callbacks(piezo_callbacks *callbacks)
{
   int rc = 0;

   if(callbacks->cmd_done_cb != NULL) {
       g_cmd_done_cb = callbacks->cmd_done_cb;
       ALOGD("%s: piezo cmd callback done \n", __func__);
   }

   return rc;
}


static int run_piezo(struct piezo_params_t const* params)
{
    int rc = 0;
    void *arg = NULL;

    ALOGD("%s: piezo hal, freq(%d), duration(%d), duty(%d)\n",
          __func__, params->frequency, params->duration, params->duty_cycle);

    pthread_mutex_lock(&g_mutex);
    rc = write_int(PIEZO_FREQUENCY_FILE, params->frequency);
    rc = write_int(PIEZO_DURATION_FILE, params->duration);
    rc = write_int(PIEZO_DUTY_CYCLE_FILE, params->duty_cycle);
    pthread_mutex_unlock(&g_mutex);

    rc = pthread_create(&g_monitor_thread, NULL, monitor_command_thread, NULL);
    ALOGD("%s: piezo hal, wait for command off\n", __func__);

    return rc;
}

static int cancel_piezo(void)
{
    int rc = 0;

    ALOGD("%s: piezo hal\n", __func__);

    if(g_monitor_thread_running == 1)
    {
       ALOGD("%s: piezo running\n", __func__);
       if (pthread_kill(g_monitor_thread, SIGUSR1) != 0) {
           ALOGE("%s:g_monitor_thread kill failure!\n", __func__);
       }
       ALOGD("%s: thread canceled \n", __func__);
       cleanup_thread();

       pthread_mutex_lock(&g_mutex);
       rc = write_int(PIEZO_COMMAND_FILE, PIEZO_COMMAND_OFF);
       pthread_mutex_unlock(&g_mutex);
    }
    else
    {
       ALOGE("%s:command on thread completed \n", __func__);
    }

    return rc;
}


static int close_module(struct piezo_device_t *dev)
{
    if (dev) {
        free(dev);
    }
    return 0;
}


/******************************************************************************/
static int open_module(const struct hw_module_t* module, char const* name,
        struct hw_device_t** device)
{
    int rc = -EINVAL;
    int fd = -1;

    ALOGD("%s: enter\n", __func__);

    pthread_once(&g_init, init_globals);

    (char *)name;

    fd = open(PIEZO_FREQUENCY_FILE, O_RDWR);

    if (fd >= 0) {
        close(fd);
        struct piezo_device_t *piezo = malloc(sizeof(struct piezo_device_t));

        if (piezo != NULL) {
            memset(piezo, 0, sizeof(*piezo));

            piezo->common.tag = HARDWARE_DEVICE_TAG;
            piezo->common.version = 0;
            piezo->common.module = (struct hw_module_t*)module;
            piezo->common.close = (int (*)(struct hw_device_t*))close_module;
            piezo->register_callbacks = register_callbacks;
            piezo->run_piezo = run_piezo;
            piezo->cancel_piezo = cancel_piezo;

            *device = (struct hw_device_t*)piezo;
            rc = 0;
        }
        else {
            rc = -ENOMEM;
        }
    }
    else {
        ALOGE("%s: no piezo HW/DevTree \n", __func__);
        rc = -EINVAL;
    }

    ALOGD("%s: exit\n", __func__);
    return rc;
}

static struct hw_module_methods_t piezo_module_methods = {
    .open =  open_module,
};


struct hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = PIEZO_HARDWARE_MODULE_ID,
    .name = "PiezoSound Module",
    .author = "Qualcomm Technologies, Inc.",
    .methods = &piezo_module_methods,
};




